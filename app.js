
var path = require('path');
var express = require('express');
var session = require('express-session');
var favicon = require('serve-favicon');
var logger = require('morgan');
var swig = require('swig');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');

var config = require('./config');

var app = express();

// View engine settings
app.engine('html', swig.renderFile);
app.set('views', config.viewsDir);
app.set('view engine', 'html');
app.set('view cache', false);
swig.setDefaults(config.swig);

// Middlewares
// Uncomment this after placing favicon in assets/
app.use(favicon(config.staticsDir + '/img/favicon/clock.png'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(config.staticsDir));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(session(config.cookie));

// Routes
app.use(require('./routes/'));

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (!config.onOpenshift) {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var server = http.createServer(app);
server.listen(config.port, config.ipAddress);
server.on('listening', function onListen() {
  console.log('Timeless Project is listening on ' + server.address().port);
});

process.on('uncaughtException', function(err) {
  console.error('uncaughtException',  err.stack);
});
