
var fs = require('fs');
var config = require('../config');

function loadAllControllers() {
  var filenamePattern = /([a-zA-Z])\.js/;
  var controllers = fs.readdirSync(config.controllersDir);
  var controllerDict = {};
  var name;
  var filename;
  
  controllers.forEach(function(item, index) {
    filename = item.split('.')[0];
    if (filenamePattern.test(item) && filename !== 'index') {
      name = filename
        .replace(/controller/i, '')
        .toLowerCase();
      controllerDict[name] = require('./' + item);
    }
  });

  return controllerDict;
}
module.exports = loadAllControllers();
