
var controllers = require('../controllers');
var express = require('express');
var router = express.Router();

router.get('/', controllers.main.index);

module.exports = router;
