
var path = require('path');

module.exports = {
  onOpenshift: (process.env.OPENSHIFT_NODEJS_PORT) ? true : false,

  port: process.env.OPENSHIFT_NODEJS_PORT || 8080,
  ipAddress: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',

  controllersDir: path.join(__dirname, '../controllers'),
  viewsDir: path.join(__dirname, '../views'),
  staticsDir: path.join(__dirname, '../assets'),

  swig: {
    cache: false
  },

  cookie: {
    name: 'timeless.sid',
    secret: 'a8cabe3958d721baa858b509b38b4154',
    resave: false,
    saveUninitialized: false
  }
};
