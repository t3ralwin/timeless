# Timeless

An openshift deployable NodeJS App.

This app uses:

* Express 4
* Swig 
* jQuery
* Bootstrap


## Creating App

I am assuming you are registered to [Openshift](https://openshift.com) and having `rhc` installed on your system. If you are new to [Openshift](https://openshift.com), pleae follow [this link](https://developers.openshift.com/en/getting-started-overview.html) to get started.

### Using Openshift Client Tools

You can create an app by using the `rhc` with following commands

```bash
rhc app create APP_NAME nodejs-0.10 --from-code=https://gitlab.com/t3ralwin/timeless.git
```

Replace `APP_NAME` with your app name and wait until the process is finished. And then a directory with name `APP_NAME` will be created. It is actually a clone of a git repository of your app which is based on a git repository that is specified by `--from-code` flag.

### Using Web Interface

* In your app list page, click `Add Application...` button.
* Choose `NodeJS-0.10` in `Other types` section.
* Give it a public url.
* Copy-paste `https://gitlab.com/t3ralwin/timeless.git` to `Source Code` field and put `master` to `Branch/tag` field.
* Leave the rest with their default value. Or you can specify them if you want.
* Click `Create Application` button.
* If it's succeed, you'll be redirected to a page that tells you what to do next.

## What Next?

Start making & commiting changes and push them to your app origin.